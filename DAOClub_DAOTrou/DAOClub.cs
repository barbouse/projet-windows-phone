﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Globalization;
using DLL_Trous_Clubs;
//using GolfLibrary.Classes.DAOClasses;



namespace DAOClub_DAOTrou
{
    public class DAOClub
    {
        private MySqlConnection conn;
        public DAOClub()
        {
            string myConnectionString;
            //myConnectionString = "server=193.252.48.172;port=20433;uid=golfede;pwd=golfede;database=golfbdd;";
            myConnectionString = "server=172.18.207.102;port=3306;uid=golfede;pwd=golfede;database=golfbdd;";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }

        public void recordAClub(club c)
        {
            // Utile pour gérer le problème de virgule qui devient point pour Mysql selon les infos de Globalization
            NumberFormatInfo nfi = new CultureInfo("fr-FR", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            // requête insert
            String myInsertQuery = "insert into club VALUES ('" + c.Code + "','" + c.Nom + "','" + c.Rue + "','" + c.Cp + "', '" + c.Ville + "','" + c.Latitude.ToString(nfi) + "','" + c.Longitude.ToString(nfi) + "','" + c.Tel + "','" + c.Email + "'," + c.Licencies + ")";
            MySqlCommand myCommand = new MySqlCommand(myInsertQuery);
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }

        public void deleteAClub(club c)
        {
            String myQuery = "delete from club where code = '" + c.Code + "'";
            MySqlCommand myCommand = new MySqlCommand(myQuery);
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }

        public List<club> getAllClubs(string saisie)
        {
            List<club> lesClubsRecuperes = new List<club>();

            string requete = "SELECT * FROM club WHERE code LIKE '%" + saisie + "%' OR nom LIKE '%" + saisie + "%' OR ville LIKE '%" + saisie + "%'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                club c = new club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Latitude = Convert.ToDouble(rdr[5].ToString());
                c.Longitude = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencies = Convert.ToInt16(rdr[9].ToString());

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }

        public club getAClubById(string id)
        {
            club c = null;
            string requete = "select * from club where code = '"+id+"'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                c = new club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Latitude = Convert.ToDouble(rdr[5].ToString());
                c.Longitude = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencies = Convert.ToInt16(rdr[9].ToString());

                
            }
            rdr.Close();
            return c;
        }
    }
}
