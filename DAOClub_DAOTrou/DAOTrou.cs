﻿using DLL_Trous_Clubs;
//using GolfLigueConsole;
//using GolfLigueConsole.Classes.TechnicalClasses;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace DAOClub_DAOTrou
{
    public class DAOTrou
    {
        private MySqlConnection conn;
        public DAOTrou()
        {
            string myConnectionString;
            //myConnectionString = "server=193.252.48.172;port=20433;uid=golfede;pwd=golfede;database=golfbdd;";
            myConnectionString = "server=172.18.207.102;port=3306;uid=golfede;pwd=golfede;database=golfbdd;";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }

        public List<trou> getAllTrousFromClub(string code)
        {
            List<trou> lesTrousRecuperes = new List<trou>();

            string requete = "select * from trou where code_club = '" + code +"'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                trou t = new trou();
                t.Numero = Convert.ToInt16(rdr[0].ToString());
                t.Distance = Convert.ToInt16(rdr[1].ToString());
                t.Par = Convert.ToInt16(rdr[2].ToString());

                lesTrousRecuperes.Add(t);
            }
            rdr.Close();
            return lesTrousRecuperes;
        }
    }
}
