﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_Trous_Clubs
{
    using System;
    using System.Collections.Generic;

    public class trou
    {

        // ATTRIBUTS + ACCESSEURS

        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private int distance;

        public int Distance
        {
            get { return distance; }
            set { distance = value; }
        }

        private int par;

        public int Par
        {
            get { return par; }
            set { par = value; }
        }

        private int score;

        public int Score
        {
            get { return score; }
            set { score = value; }
        }
        

        // CONSTRUCTEUR
        public trou(int unnumero, int undistance, int unpar)
        {
            this.numero = unnumero;
            this.distance = undistance;
            this.par = unpar;
            this.score = 0;
        }

        public trou()
        {
          

        }


        public string ToString()
        {
            return "TROU N° : " + this.numero + " ---- " + this.distance + "m ---- PAR : " + this.par;
        }


    }

}
