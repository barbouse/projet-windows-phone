﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_Trous_Clubs
{
    using System;
    using System.Collections.Generic;

    public class club
    {
 
        // ATTRIBUTS + ACCESSEURS
      
        private System.Collections.Generic.List<trou> lesTrous;

        public System.Collections.Generic.List<trou> LesTrous
        {
            get { return lesTrous; }
            set { lesTrous = value; }
        }
  
        private string code;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        private string nom;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        private string rue;

        public string Rue
        {
            get { return rue; }
            set { rue = value; }
        }

        private string cp;

        public string Cp
        {
            get { return cp; }
            set { cp = value; }
        }

        private string ville;

        public string Ville
        {
            get { return ville; }
            set { ville = value; }
        }

        private double latitude;

        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        private double longitude;

        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        private string tel;

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private int licencies;

        public int Licencies
        {
            get { return licencies; }
            set { licencies = value; }
        }

   
        // CONSTRUCTEUR

        public club(string uncode, string unnom, string unrue, string uncp, string unville, double ungps_lat, double ungps_lon, string untel, string unemail, int unlicencie)
        {
            this.code = uncode;
            this.nom = unnom;
            this.rue = unrue;
            this.cp = uncp;
            this.ville = unville;
            this.latitude = ungps_lat;
            this.longitude = ungps_lon;
            this.tel = untel;
            this.email = unemail;
            this.licencies = unlicencie;
        }

        public club()
        {


        }


        public void setLesTrous(List<trou> liste)
        {
            this.lesTrous = liste;
        }

        public List<trou> getLesTrous()
        {
            return this.lesTrous;
        }

        public void ajouterTrou(trou t)
        {
            this.lesTrous.Add(t);
        }

        public string ToString()
        {
            return this.code + " " + this.nom + " " + this.licencies + " licenciés";
        }

  

      
    }

}
