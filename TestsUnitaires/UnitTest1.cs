﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Appli_Windows_Phone;  // le using du projet sur lequel on fait les tests unitaires

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLL_Trous_Clubs;



namespace TestsUnitaires
{
    [TestClass]
    public class UnitTest1
    {
		
		[TestMethod]
        public void Test_setLesTrous()   // setLesTrous : Méthode de club
        {
			club leclub = new club("codetest", "nomtest", "ruetest", "00000", "villetest", 145.23, 4589.6, "teltest", "mailtest", 26);
      
			trou letrou1 = new trou(1, 100, 5);
			trou letrou2 = new trou(2, 200, 10);
			trou letrou3 = new trou(3, 300, 15);
	  
			List<trou> lestrous = new List<trou>();
			
			lestrous.Add(letrou1);
			lestrous.Add(letrou2);
			lestrous.Add(letrou3);
			
			leclub.setLesTrous(lestrous);
			
			Assert.AreEqual(leclub.LesTrous.Count, 3, 0, "Tous les trous n'ont pas été insérés !");

            Assert.AreEqual(leclub.LesTrous[0].Distance, 100, 0, "Problèmes dans les attributs de certains trous !");
        }


        [TestMethod]
        public void Test_trou_ToString()
        {
            club leclub = new club("codetest", "nomtest", "ruetest", "00000", "villetest", 145.23, 4589.6, "teltest", "mailtest", 26);
      
			trou letrou1 = new trou(1, 100, 5);
			trou letrou2 = new trou(2, 200, 10);
			trou letrou3 = new trou(3, 300, 15);

            List<trou> lestrous = new List<trou>();
			
			lestrous.Add(letrou1);
			lestrous.Add(letrou2);
			lestrous.Add(letrou3);

            string string1 = leclub.LesTrous.ElementAt(0).ToString();
			string string2 = " TROU N° : 1 ---- 100m ---- PAR : 5";
				
			Assert.AreEqual(string1, string2, false, "Problème sur ToString() de la classe trou : les chaînes ne sont pas identiques !");
        }

        
        [TestMethod]
        public void Test_getAllClubs()
        {
            DAOClub daoclub = new DAOClub();

            List<club> lesclubs = daoclub.getAllClubs("C0001", "", "");

            club leclub = new club("C0001", "Golf de Combles", "38 rue Basse", "55000", "COMBLES EN BARROIS", 48.7582240, 5.1153370, "0329845151", "siogolfclubslam@sio.fr", 175);
            leclub.LesTrous.Add(new trou(1, 350, 3));
            leclub.LesTrous.Add(new trou(2, 510, 5));
            leclub.LesTrous.Add(new trou(3, 423, 4));

            Assert.AreEqual(lesclubs[0], leclub, "Problème sur getAllClubs() et/ou getAllTrousFromClub() : les clubs ne sont pas identiques !");
        }

       			
				
	


    }
}










