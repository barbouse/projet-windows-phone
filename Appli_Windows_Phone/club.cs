﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appli_Windows_Phone
{
    using System;
    using System.Collections.Generic;

    public class club
    {
  
        public string code { get; set; }
        public string nom { get; set; }
        public string rue { get; set; }
        public string cp { get; set; }
        public string ville { get; set; }
        public string gps_lat { get; set; }
        public string gps_lon { get; set; }
        public string tel { get; set; }
        public string email { get; set; }
        public int licencie { get; set; }

        public club(string uncode, string unnom, string unrue, string uncp, string unville, string ungps_lat, string ungps_lon, string untel, string unemail, int unlicencie)
        {
            this.code = uncode;
            this.nom = unnom;
            this.rue = unrue;
            this.cp = uncp;
            this.ville = unville;
            this.gps_lat = ungps_lat;
            this.gps_lon = ungps_lon;
            this.tel = untel;
            this.email = unemail;
            this.licencie = unlicencie;
        }

  

      
    }

}
