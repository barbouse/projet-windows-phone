﻿using DLL_Trous_Clubs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d'élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkId=391641

namespace Appli_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static List<club> lesclubs = new List<club>();
        public static List<trou> lestrous = new List<trou>();
		public static club leclub = null;
        public static string login = "";

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            // TEST
            
          
                
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d’événement décrivant la manière dont l’utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: préparer la page pour affichage ici.

            // TODO: si votre application comporte plusieurs pages, assurez-vous que vous
            // gérez le bouton Retour physique en vous inscrivant à l’événement
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Si vous utilisez le NavigationHelper fourni par certains modèles,
            // cet événement est géré automatiquement.
        }

        private async void b_co(object sender, RoutedEventArgs e)
        {
            // On récupère les données saisies par l'utilisateur
				string pseudo = tb_nom.Text;
				string pass = tb_pass.Password;
				
			// On envoie ces données à un script php distant qui se chargera de checker les informations de connexion
                using (var httpClient = new HttpClient())
                {
                    /*
                        // classe utilisée pour sérialiser des instances en JSON - ici une List de club est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(club));  // modif faite sur typeof() pour récupérer un club
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise le club et le copie dans le stream en mémoire.
                        //serializer.WriteObject(stream_post, MainPage.leclub);

                        serializer.WriteObject(stream_post, MainPage.leclub);

                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées
                    */

                
                        // flux dont le contenu est en mémoire
                            var stream_post = new MemoryStream();
                        // on se cale au début du flux
                            stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                            StreamReader sr = new StreamReader(stream_post);



                    // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                    httpClient.BaseAddress = new Uri("http://localhost/script_php_distant/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // exemple montrant un envoi de données clés valeurs sous forme de texte
                    var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("login", pseudo),
                        new KeyValuePair<string, string>("pass", pass)
                    };

                    // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                    var content = new FormUrlEncodedContent(pairs);

                    // formule l'appel au script distant en lui passant les données en POST
                    // TESTER LE TEST DISPO SERVEUR CI-DESSOUS
                    // EN CAS DE SUPPRESSION, CONSERVER LE CONTENU DU IF EN TANT QUE LIGNE DE CODE

                    //HttpResponseMessage response = "ko";
                    //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;



                    HttpResponseMessage response = httpClient.PostAsync("verif_co.php", content).Result;

                    string statusCode = response.StatusCode.ToString();



                    if (statusCode != "NotFound")
                    {

                        // >>>>>>>>    HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;

                        // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

                        // DEBUG
                        //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                        // >>>>>>>>   string statusCode = response.StatusCode.ToString();


                        response.EnsureSuccessStatusCode();
                        Task<string> responseBody = response.Content.ReadAsStringAsync();

                        
                            // Désérialisation
                            var jsonSerializer = new DataContractJsonSerializer(typeof(Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            Message m = (Message)jsonSerializer.ReadObject(stream);
                        

                       
                        string retour_script = m.Libelle;

                        //TB_result.Text = response.StatusCode.ToString();   // Affiche 'OK' si la requête c'est bien passée


                        // à la validation, on change l'état de la note courante afin que le système en crée une nouvelle aux prochains frais déclarés
                        //MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1).valider();

                        // Actualisation du fichier local
                        //await writeJsonAsync();
                        //await readJsonAsync();
                        //await deleteJsonAsync();


                        if(retour_script.Equals("OK"))
                        {
                            MainPage.login = pseudo;
                            this.Frame.Navigate(typeof(MainMenu));
                        }
                        else
                        {
                            MessageDialog msgbox1 = new MessageDialog("Saisie Incorrecte !");
                            //Calling the Show method of MessageDialog class  
                            //which will show the MessageBox  
                            await msgbox1.ShowAsync();
                        }
            

                    }
                    else
                    {
                        MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible !");
                        //Calling the Show method of MessageDialog class  
                        //which will show the MessageBox  
                        await msgbox1.ShowAsync();
                    }
                }
            
            
              
        }


        // Quitter
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
