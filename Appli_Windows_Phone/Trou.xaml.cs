﻿using DLL_Trous_Clubs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace Appli_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Trou : Page
    {
		private const string JSONFILENAME = "data.json";
		
		public Trou()
        {
            this.InitializeComponent();

			// On récupère le club du fichier local et on le met dans MainPage.leclub
				chargerData();

		
			// On met les numéros des trous de MainPage.leclub dans la combobox N°Trou
				foreach(trou letrou in MainPage.leclub.LesTrous)
					cb_num_trou.Items.Add(letrou.Numero);
           

            // On affiche le club en cours dans tb_club
                tb_infoclub2.Text = MainPage.leclub.Nom;
        }
		
		
        private async Task writeJsonAsync()   // ECRIRE DANS LE FICHIER LOCAL
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var serializer = new DataContractJsonSerializer(typeof(club));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, MainPage.leclub);
            }

            //tb_result.Text = "Write succeeded";
        }


        private async Task deleteJsonAsync()     //     SUPPRESSION DU CONTENU DU FICHIER LOCAL
        {
            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }
        }

		
        public async void chargerData()
        {
            await deserializeJsonAsync();
        }
		
		
        private async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                club leclub = null;

                var jsonSerializer = new DataContractJsonSerializer(typeof(club));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                leclub = (club)jsonSerializer.ReadObject(myStream);

				/*
					foreach (var n in leclub)
					{
						content += n.ToString() + Environment.NewLine;
					}
				*/
				
                MainPage.leclub = leclub;
            }
            catch (FileNotFoundException)
            {
                // If the file dosn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }

            //tb_result.Text = content;
        }
		
		
        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

		
		
        // Click sur bouton "enregistrer ce trou"
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cb_num_trou.SelectedIndex != -1 && tb_coups.Text != "")
            {
                // Récupération des données saisies par l'utilisateur
                    string num = cb_num_trou.SelectedItem.ToString();
                    string scor = tb_coups.Text;

                if (!num.Equals("") && !scor.Equals("") && !scor.Equals("0"))
                {
                    // Récupération des données saisies par l'utilisateur
                    int num_trou = Convert.ToInt16(cb_num_trou.SelectedItem.ToString());
                    int score = Convert.ToInt16(tb_coups.Text);

                    // On enregistre le score dans MainPage.leclub
                    foreach (trou letrou in MainPage.leclub.LesTrous)
                    {
                        if (letrou.Numero == num_trou)
                        {
                            letrou.Score = score;
                            tb_score_renseigne.Text = "Score renseigné : " + score;
                        }
                    }

                    // On remet le club dans le fichier local
                    await writeJsonAsync();
                }
                else
                {
                    MessageDialog msgbox1 = new MessageDialog("Saisie Incorrecte !");
                    //Calling the Show method of MessageDialog class  
                    //which will show the MessageBox  
                    await msgbox1.ShowAsync();
                }
            }
            else
            {
                MessageDialog msgbox1 = new MessageDialog("Renseignez tous les champs !");
                //Calling the Show method of MessageDialog class  
                //which will show the MessageBox  
                await msgbox1.ShowAsync();
            }
        }


        // Click sur bouton "valider le parcours"
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // On vérifie que chaque trou de MainPage.leclub a bien l'attribut score renseigné
				int check = 1;
				
				foreach(trou letrou in MainPage.leclub.LesTrous)
				{
					if(letrou.Score == 0)
						check = 0;
				}
				
				if(check == 1)
				{
					// On envoie MainPage.leclub au script php qui se charge d'enregistrer les scores (voir tp web_services)

                    using (var httpClient = new HttpClient())
                    {
                        // classe utilisée pour sérialiser des instances en JSON - ici un club est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(club));  // modif faite sur typeof() pour récupérer un club
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise la collection de notes et la copie dans le stream en mémoire.
                        //serializer.WriteObject(stream_post, MainPage.lesNotes);

                        serializer.WriteObject(stream_post, MainPage.leclub);

                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient.BaseAddress = new Uri("http://localhost/script_php_distant/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("login", MainPage.login),
                            new KeyValuePair<string, string>("data", sr.ReadToEnd())
                        };

                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        // TESTER LE TEST DISPO SERVEUR CI-DESSOUS
                        // EN CAS DE SUPPRESSION, CONSERVER LE CONTENU DU IF EN TANT QUE LIGNE DE CODE

                        //HttpResponseMessage response = "ko";
                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;



                        HttpResponseMessage response = httpClient.PostAsync("sauvegarde.php", content).Result;

                        string statusCode = response.StatusCode.ToString();



                        if (statusCode != "NotFound")
                        {

                                                // >>>>>>>>    HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;

                                                // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

                                                // DEBUG
                                                //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


                                                //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                                                // >>>>>>>>   string statusCode = response.StatusCode.ToString();


                            response.EnsureSuccessStatusCode();
                            Task<string> responseBody = response.Content.ReadAsStringAsync();

                            // Désérialisation
                            var jsonSerializer = new DataContractJsonSerializer(typeof(Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            Message m = (Message)jsonSerializer.ReadObject(stream);

                            //tb_result.Text = m.Libelle;

                                                //TB_result.Text = response.StatusCode.ToString();   // Affiche 'OK' si la requête c'est bien passée

                            
                                                // à la validation, on change l'état de la note courante afin que le système en crée une nouvelle aux prochains frais déclarés
                                                //MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1).valider();
                            
                                                // Actualisation du fichier local
                                                //await writeJsonAsync();
                                                //await readJsonAsync();
                                                //await deleteJsonAsync();

                            // Si la sauvegarde s'est effectuée, on efface le fichier local ainsi que le contenu de la collection MainPage.leclub et on redirige le user vers MainMenu
                                MainPage.leclub = null;
                                await deleteJsonAsync();
                                this.Frame.Navigate(typeof(MainMenu));
                        }
                        else
                        {
                            MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible !");
                            //Calling the Show method of MessageDialog class  
                            //which will show the MessageBox  
                            await msgbox1.ShowAsync();
                        }
                    }

				}
				else
				{
                    MessageDialog msgbox1 = new MessageDialog("Chaque trou doit avoir un score renseigné !");
                    //Calling the Show method of MessageDialog class  
                    //which will show the MessageBox  
                    await msgbox1.ShowAsync();
				}
        }


        // Retour Page Précédente
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private async void selec_trou_combobox(object sender, SelectionChangedEventArgs e)
        {
            int num_select = Convert.ToInt16(cb_num_trou.SelectedItem);

            foreach(trou letrou in MainPage.leclub.LesTrous)
            {
                if(letrou.Numero == num_select)
                {
                    tb_distance.Text = "Distance : " + letrou.Distance.ToString();
                    tb_par.Text = "PAR : " + letrou.Par.ToString();

                    if (letrou.Score == 0)
                        tb_score_renseigne.Text = "Score non renseigné";
                    else
                        tb_score_renseigne.Text = "Score renseigné : " + letrou.Score.ToString();
                }
            }
        }


        
    }
}
