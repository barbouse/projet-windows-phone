﻿using DLL_Trous_Clubs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace Appli_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainMenu : Page
    {
        private const string JSONFILENAME = "data.json";
        public MainMenu()
        {
            this.InitializeComponent();
            
            chargerData();


        }


        public async void chargerData()
        {
            await deserializeJsonAsync();
        }


        private async Task writeJsonAsync()   // ECRIRE DANS LE FICHIER LOCAL
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var serializer = new DataContractJsonSerializer(typeof(club));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, MainPage.leclub);
            }

            //tb_result.Text = "Write succeeded";
        }

        private async Task readJsonAsync()   // LIRE LE FICHIER LOCAL
        {
            string content = String.Empty;

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);
            using (StreamReader reader = new StreamReader(myStream))
            {
                content = await reader.ReadToEndAsync();
            }

            //tb_result.Text = content;
        }

        private async Task deleteJsonAsync()     //     SUPPRESSION DU CONTENU DU FICHIER LOCAL
        {
            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }
        }

        private async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                club leclub = null;

                var jsonSerializer = new DataContractJsonSerializer(typeof(club));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                leclub = (club)jsonSerializer.ReadObject(myStream);

                /*
                    foreach (var n in leclub)
                    {
                        content += n.ToString() + Environment.NewLine;
                    }
                */

                MainPage.leclub = leclub;

                // On affiche le club en cours dans tb_club s'il y en a un
                if (MainPage.leclub == null)
                    tb_infoclub1.Text = "Aucun club en cours";
                else
                    tb_infoclub1.Text = "En cours : " + MainPage.leclub.Nom;
            }
            catch (FileNotFoundException)
            {
                // If the file dosn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }

            //tb_result.Text = content;
        }










        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void b_(object sender, RoutedEventArgs e)
        {

        }

        private void b_commencer(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Selection_club));
            
        }


        // Valider parcours en cours
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
             // On vérifie que MainPage.leclub != null   
             if(MainPage.leclub != null)
             {
                // On vérifie que chaque trou de MainPage.leclub a bien l'attribut score renseigné
                int check = 1;

                foreach (trou letrou in MainPage.leclub.LesTrous)
                {
                    if (letrou.Score == 0)
                        check = 0;
                }

                if (check == 1)
                {
                    // On envoie MainPage.leclub au script php qui se charge d'enregistrer les scores (voir tp web_services)

                    using (var httpClient = new HttpClient())
                    {
                        // classe utilisée pour sérialiser des instances en JSON - ici un club est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(club));  // modif faite sur typeof() pour récupérer un club
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise la collection de notes et la copie dans le stream en mémoire.
                        //serializer.WriteObject(stream_post, MainPage.lesNotes);

                        serializer.WriteObject(stream_post, MainPage.leclub);

                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient.BaseAddress = new Uri("http://localhost/script_php_distant/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("login", MainPage.login),
                            new KeyValuePair<string, string>("data", sr.ReadToEnd())
                        };

                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        // TESTER LE TEST DISPO SERVEUR CI-DESSOUS
                        // EN CAS DE SUPPRESSION, CONSERVER LE CONTENU DU IF EN TANT QUE LIGNE DE CODE

                        //HttpResponseMessage response = "ko";
                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;



                        HttpResponseMessage response = httpClient.PostAsync("sauvegarde.php", content).Result;

                        string statusCode = response.StatusCode.ToString();



                        if (statusCode != "NotFound")
                        {

                            // >>>>>>>>    HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;

                            // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

                            // DEBUG
                            //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


                            //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                            // >>>>>>>>   string statusCode = response.StatusCode.ToString();


                            response.EnsureSuccessStatusCode();
                            Task<string> responseBody = response.Content.ReadAsStringAsync();

                            // Désérialisation
                            var jsonSerializer = new DataContractJsonSerializer(typeof(Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            Message m = (Message)jsonSerializer.ReadObject(stream);

                            //tb_result.Text = m.Libelle;

                            //TB_result.Text = response.StatusCode.ToString();   // Affiche 'OK' si la requête c'est bien passée


                            // à la validation, on change l'état de la note courante afin que le système en crée une nouvelle aux prochains frais déclarés
                            //MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1).valider();

                            // Actualisation du fichier local
                            //await writeJsonAsync();
                            //await readJsonAsync();
                            //await deleteJsonAsync();

                            // Si la sauvegarde s'est effectuée, on efface le fichier local ainsi que le contenu de la collection MainPage.leclub et on redirige le user vers MainMenu
                            MainPage.leclub = null;
                            await deleteJsonAsync();
                            this.Frame.Navigate(typeof(MainMenu));
                        }
                        else
                        {
                            MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible !");
                            //Calling the Show method of MessageDialog class  
                            //which will show the MessageBox  
                            await msgbox1.ShowAsync();
                        }
                    }

                }
                else
                {
                    MessageDialog msgbox1 = new MessageDialog("Chaque trou doit avoir un score renseigné !");
                    //Calling the Show method of MessageDialog class  
                    //which will show the MessageBox  
                    await msgbox1.ShowAsync();
                }
            }
             else
             {
                 MessageDialog msgbox1 = new MessageDialog("Vous n'avez aucun club en cours de saisie !");
                 //Calling the Show method of MessageDialog class  
                 //which will show the MessageBox  
                 await msgbox1.ShowAsync();
             }

        }


        // Retour aux trous
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (MainPage.leclub != null)
                this.Frame.Navigate(typeof(Trou));
            else
            {
                MessageDialog msgbox1 = new MessageDialog("Vous n'avez aucun club en cours de saisie !");
                //Calling the Show method of MessageDialog class  
                //which will show the MessageBox  
                await msgbox1.ShowAsync();

                this.Frame.Navigate(typeof(Selection_club));
            }
        }


        // Quitter
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
		
		
    /*
		// ANCIENNE METHODE NOTE DE FRAIS    APPUIE SUR SAUVEGARDER EN LOCAL
		private async void Button_Click_3(object sender, RoutedEventArgs e)
        {
            await writeJsonAsync();
            //await deserializeJsonAsync();
            await readJsonAsync();
        }
	
		
		
		// ANCIENNE METHODE NOTE DE FRAIS     VALIDATION ---> ENVOI A UN SCRIPT PHP DISTANT QUI ENREGISTRE DANS LA BDD
        private async void Button_Click_4(object sender, RoutedEventArgs e)
        {
            
            if ((MainPage.lesNotes == null) || (MainPage.lesNotes.Count == 0) || (MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1).etat.Equals("transmise")))
            {
                TB_result.Text = "Aucune Note n'est actuellement ouverte.";
            }
            else
            {
                using (var httpClient = new HttpClient())
                {
                    // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                    var serializer = new DataContractJsonSerializer(typeof(Note));  // modif faite sur typeof() pour récupérer une note (tous le code ne concerne donc que la DERNIERE note) ATTENTION DONC LORS DE LA LECTURE DES COMMENTAIRES
                    // flux dont le contenu est en mémoire
                    var stream_post = new MemoryStream();
                    // sérialise la collection de notes et la copie dans le stream en mémoire.
                    //serializer.WriteObject(stream_post, MainPage.lesNotes);

                    serializer.WriteObject(stream_post, MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1));

                    // on se cale au début du flux
                    stream_post.Position = 0;
                    // on crée un flux de lecture à partir de celui en mémoire
                    StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                    // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                    httpClient.BaseAddress = new Uri("http://localhost/ws_phone_test/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // exemple montrant un envoi de données clés valeurs sous forme de texte
                    var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("login", "toto"),
                        new KeyValuePair<string, string>("mdp", "toto"),
                        new KeyValuePair<string, string>("data", sr.ReadToEnd())
                    };

                    // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                    var content = new FormUrlEncodedContent(pairs);

                    // formule l'appel au script distant en lui passant les données en POST
                    // TESTER LE TEST DISPO SERVEUR CI-DESSOUS
					// EN CAS DE SUPPRESSION, CONSERVER LE CONTENU DU IF EN TANT QUE LIGNE DE CODE

                    //HttpResponseMessage response = "ko";
                    //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;



                    HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;

                    string statusCode = response.StatusCode.ToString();

	                    

					if(statusCode != "NotFound")
					{
                             
                        // >>>>>>>>    HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;
                            
						// BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

						// DEBUG
						//System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


						//HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

						// >>>>>>>>   string statusCode = response.StatusCode.ToString();


						response.EnsureSuccessStatusCode();
						Task<string> responseBody = response.Content.ReadAsStringAsync();

						// Désérialisation
						var jsonSerializer = new DataContractJsonSerializer(typeof(Message));
						var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
						Message m = (Message)jsonSerializer.ReadObject(stream);

						TB_result.Text = m.Libelle;

						//TB_result.Text = response.StatusCode.ToString();   // Affiche 'OK' si la requête c'est bien passée
							
							
						// à la validation, on change l'état de la note courante afin que le système en crée une nouvelle aux prochains frais déclarés
						MainPage.lesNotes.ElementAt(MainPage.lesNotes.Count - 1).valider();
							
						// Actualisation du fichier local
						    await writeJsonAsync();
						    await readJsonAsync();
                            await deleteJsonAsync();
					}
					else
					{
                        MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible !");
                        //Calling the Show method of MessageDialog class  
                        //which will show the MessageBox  
                        await msgbox1.ShowAsync(); 
					}
                }
            }

           


        }
    */

    }
}
