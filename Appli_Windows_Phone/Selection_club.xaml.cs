﻿using DLL_Trous_Clubs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace Appli_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Selection_club : Page
    {
        private const string JSONFILENAME = "data.json";
		
		public Selection_club()
        {
            this.InitializeComponent();

        }

		
        public async void chargerData()
        {
            await deserializeJsonAsync();
        }

        private async Task writeJsonAsync()   // ECRIRE DANS LE FICHIER LOCAL
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var serializer = new DataContractJsonSerializer(typeof(club));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, MainPage.leclub);
            }

            //tb_result2.Text = "Write succeeded";
        }

        private async Task readJsonAsync()   // LIRE LE FICHIER LOCAL
        {
            string content = String.Empty;

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);
            using (StreamReader reader = new StreamReader(myStream))
            {
                content = await reader.ReadToEndAsync();
            }

            //tb_result2.Text = content;
        }

        private async Task deleteJsonAsync()     //     SUPPRESSION DU CONTENU DU FICHIER LOCAL
        {
            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }
        }

        private async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                List<club> mes_notes;

                var jsonSerializer = new DataContractJsonSerializer(typeof(List<club>));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                mes_notes = (List<club>)jsonSerializer.ReadObject(myStream);

                foreach (var n in mes_notes)
                {
                    content += n.ToString() + Environment.NewLine;
                }
                MainPage.lesclubs = mes_notes;
            }
            catch (FileNotFoundException)
            {
                // If the file doesn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }

            //tb_result2.Text = content;
        }
		
		
		
		
        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        // Bouton Rechercher
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
                // Reset listbox
                    lb.Items.Clear();
            
                // Réinitialisation de la liste des clubs
                    MainPage.lesclubs = new List<club>();


                // Récupération de la saisie de l'utilisateur
                    string saisie = tb_saisie.Text;
     

                // Contact du webservice pour qu'il renvoie la chaîne JSON des clubs correspondants aux critères de recherche de l'utilisateur
            
                    var http_Client = new HttpClient();

                    http_Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                
					http_Client.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/get_clubs");


                    var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><get_clubs xmlns=\"http://tempuri.org/\"><saisie>" + saisie + "</saisie></get_clubs></soap:Body></soap:Envelope>";
				
				
				
				
				/*
				
				// TEST AVEC HelloWorld()
				
					httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/HelloWorld");

    
                    var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><HelloWorld xmlns=\"http://tempuri.org/\" /></soap:Body></soap:Envelope>";

 
				
				///////////////////////////
				
				*/
				
				
				
				// Récupération de la chaîne retour du WebService (return d'une chaîne simple)

                    var response_ = http_Client.PostAsync("http://172.18.207.204/WS_LLA.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
                    
                    var content_ = response_.Content.ReadAsStringAsync().Result;
                 

                
            /*
                    // TEST
                        tb_result2.Text = content;
                    //////////
            */
                
            // TEST
                    System.Diagnostics.Debug.WriteLine(content_);
            



                    XDocument responseXML = XDocument.Parse(content_);

                    String retour = responseXML.Root.Value;

                    // tb_result2.Text = retour;
           

              
                 

			/*
             		// Gestion de retour si plusieurs tableaux sont renvoyés
					// En effet, dans ce cas, la chaîne renvoyée contient du code xml afin de séparer chaque tableau des autres.
					// Il faut donc parcourir les XElement dans le but de récupérer les tableaux un par un
			
                    //IEnumerable<XNode> liste = responseXML.Nodes();
                    IEnumerable<XElement> liste = responseXML.Root.Descendants("{http://tempuri.org/}string");
                    
                    string v = "";

                    foreach (XElement element in liste)
                    {
                        v += element.Value;
                        System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - element : " + element.Name + " " + element.Value);
                    }

                    tb_result2.Text = v;
           */

           
			// Suite normale du code

                    string clubs_webservice = retour;

                    //string clubs_webservice = "[{\"Code\":\"C0001\",\"Nom\":\"SLAM golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogo lfclubslam@sio.fr\",\"Licencie\":14},{\"Code\":\"C0002\",\"Nom\":\"SISR golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogo lfclubsisr@sio.fr\",\"Licencie\":12},{\"Code\":\"C0003\",\"Nom\":\"SIO1 golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogo lfclubsio1@sio.fr\",\"Licencie\":33}]";
                    
                    //string clubs_webservice = '[{"Code":"C0001","Nom":"SLAM golf Club","Rue":"rue de la prairie","Cp":"55000","Ville":"BAR LE DUC","Gps_lat":48.758224,"Gps_lon":5.115337,"Tel":"0329845151","Email":"siogo lfclubslam@sio.fr","Licencie":14},{"Code":"C0002","Nom":"SISR golf Club","Rue":"rue de la prairie","Cp":"55000","Ville":"BAR LE DUC","Gps_lat":48.758224,"Gps_lon":5.115337,"Tel":"0329845151","Email":"siogo lfclubsisr@sio.fr","Licencie":12},{"Code":"C0003","Nom":"SIO1 golf Club","Rue":"rue de la prairie","Cp":"55000","Ville":"BAR LE DUC","Gps_lat":48.758224,"Gps_lon":5.115337,"Tel":"0329845151","Email":"siogo lfclubsio1@sio.fr","Licencie":33}]';


                // Désérialisation de la chaîne JSON et création des objets club que l'on stocke dans MainPage.lesclubs

                    var stream = new MemoryStream(Encoding.UTF8.GetBytes(clubs_webservice));
                    var jsonSerializer = new DataContractJsonSerializer(typeof(List<club>));

   
                    foreach (club leclub in (List<club>)jsonSerializer.ReadObject(stream))
                    {
                        MainPage.lesclubs.Add(leclub);
                    }
                
      

                /* TEST
                    List<club> truc = new List<club>();
                    foreach(club leclub in MainPage.lesclubs)
                    {
                        truc.Add(leclub);
                    }
                */
                

                // Affichage des clubs trouvés
                    foreach (club leclub in MainPage.lesclubs)
                    {
                        // On affiche le club dans la listbox sous la forme : 'nomclub_codeclub' :
                        lb.Items.Add(leclub.Nom + "_" + leclub.Code);
                    }
					
				




				// On insère les clubs (et leurs trous) de MainPage.lesclubs dans la BDD s'ils n'y sont pas déjà
					using (var httpClient = new HttpClient())
					{
						// classe utilisée pour sérialiser des instances en JSON - ici une liste de clubs est spécifiée pour indiquer les data à sérialiser.
						var serializer = new DataContractJsonSerializer(typeof(List<club>));
						// flux dont le contenu est en mémoire
						var stream_post = new MemoryStream();
						// sérialise la collection de notes et la copie dans le stream en mémoire.
						// serializer.WriteObject(stream_post, MainPage.lesNotes);

						serializer.WriteObject(stream_post, MainPage.lesclubs);

						// on se cale au début du flux
						stream_post.Position = 0;
						// on crée un flux de lecture à partir de celui en mémoire
						StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

						// on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
						httpClient.BaseAddress = new Uri("http://localhost/script_php_distant/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
						httpClient.DefaultRequestHeaders.Accept.Clear();
						httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

						// exemple montrant un envoi de données clés valeurs sous forme de texte
						var pairs = new List<KeyValuePair<string, string>>
						{
							new KeyValuePair<string, string>("data", sr.ReadToEnd())
						};

						// on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
						var content = new FormUrlEncodedContent(pairs);

						HttpResponseMessage response = httpClient.PostAsync("enregistrer_clubs.php", content).Result;

						string statusCode = response.StatusCode.ToString();

						
						if(statusCode != "NotFound")
						{
							// BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['data']

							response.EnsureSuccessStatusCode();
							Task<string> responseBody = response.Content.ReadAsStringAsync();

							// Désérialisation
							jsonSerializer = new DataContractJsonSerializer(typeof(Message));
							stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
							Message m = (Message)jsonSerializer.ReadObject(stream);

							//tb_result2.Text = m.Libelle;

						}
						else
						{
							MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible HIHI!");
							//Calling the Show method of MessageDialog class  
							//which will show the MessageBox  
							await msgbox1.ShowAsync(); 
						}
					}



                
        }


        // Click sur un club de la liste
        private async void click_club_liste(object sender, RoutedEventArgs e)
        {
            // Réinitialisation de MainPage.leclub
                MainPage.leclub = null;


            // Récupération du code du club sélectionné
                string code_club_non_splitted = lb.SelectedItem.ToString();
                string code_club = code_club_non_splitted.Split('_')[1];


            // On récupère le club correspondant depuis MainPage.lesclubs et on le met dans MainPage.leclub
                foreach (club leclub in MainPage.lesclubs)
                {
                    if (leclub.Code.Equals(code_club))
                    {
                        MainPage.leclub = leclub;
                    }
                }



            // Contact du webservice pour qu'il renvoie la chaîne JSON du club nourri de ses trous correspondant à la sélection de l'utilisateur dans la listbox

                var http_Client = new HttpClient();

                http_Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));


                http_Client.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/get_trous_dun_club");


                var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><get_trous_dun_club xmlns=\"http://tempuri.org/\"><code>" + code_club + "</code></get_trous_dun_club></soap:Body></soap:Envelope>";



            // Récupération de la chaîne retour du WebService (return d'une chaîne simple)

            var response_ = http_Client.PostAsync("http://172.18.207.204/WS_LLA.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;

            var content_ = response_.Content.ReadAsStringAsync().Result;

            XDocument responseXML = XDocument.Parse(content_);

            String retour = responseXML.Root.Value;

          

            string trous_webservice = retour;


            // Désérialisation de la chaîne JSON et création des objets trou que l'on stocke dans MainPage.leclub

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(trous_webservice));
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<trou>));

            /*
            List<trou> lesTrousRecuperes = new List<trou>();

            lesTrousRecuperes = (List<trou>)jsonSerializer.ReadObject(stream);

            MainPage.leclub.LesTrous = lesTrousRecuperes;
            */


            MainPage.leclub.setLesTrous((List<trou>)jsonSerializer.ReadObject(stream));
            /*
            foreach (trou letrou in (List<trou>)jsonSerializer.ReadObject(stream))
            {
                MainPage.leclub.LesTrous.Add(letrou);
            }
            */
	
				
			// On enregistre MainPage.leclub dans le fichier local
				await writeJsonAsync();










                // On insère les trous du club sélectionné dans la BDD s'ils n'y sont pas déjà
                using (var httpClient = new HttpClient())
                {
                    // classe utilisée pour sérialiser des instances en JSON - ici une liste de clubs est spécifiée pour indiquer les data à sérialiser.
                    var serializer = new DataContractJsonSerializer(typeof(List<trou>));
                    // flux dont le contenu est en mémoire
                    var stream_post = new MemoryStream();
                    // sérialise la collection de notes et la copie dans le stream en mémoire.
                    // serializer.WriteObject(stream_post, MainPage.lesNotes);

                    serializer.WriteObject(stream_post, MainPage.leclub.LesTrous);

                    // on se cale au début du flux
                    stream_post.Position = 0;
                    // on crée un flux de lecture à partir de celui en mémoire
                    StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                    // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                    httpClient.BaseAddress = new Uri("http://localhost/script_php_distant/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // exemple montrant un envoi de données clés valeurs sous forme de texte
                    var pairs = new List<KeyValuePair<string, string>>
						{
							new KeyValuePair<string, string>("data", sr.ReadToEnd()),
                            new KeyValuePair<string, string>("code_club", MainPage.leclub.Code)
						};

                    // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                    var content = new FormUrlEncodedContent(pairs);

                    HttpResponseMessage response = httpClient.PostAsync("enregistrer_trous.php", content).Result;

                    string statusCode = response.StatusCode.ToString();


                    if (statusCode != "NotFound")
                    {
                        // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['data']

                        response.EnsureSuccessStatusCode();
                        Task<string> responseBody = response.Content.ReadAsStringAsync();

                        // Désérialisation
                        jsonSerializer = new DataContractJsonSerializer(typeof(Message));
                        stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                        Message m = (Message)jsonSerializer.ReadObject(stream);

                        //tb_result2.Text = m.Libelle;

                    }
                    else
                    {
                        MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible HIHI!");
                        //Calling the Show method of MessageDialog class  
                        //which will show the MessageBox  
                        await msgbox1.ShowAsync();
                    }
                }









				
				
            // Redirection vers la page des trous
                this.Frame.Navigate(typeof(Trou));
        }


        // Retour Page Précédente
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
