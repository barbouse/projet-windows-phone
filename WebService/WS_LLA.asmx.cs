﻿using DAOClub_DAOTrou;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLL_Trous_Clubs;
using System.Web.Script.Serialization;

namespace WebService
{
    /// <summary>
    /// Description résumée de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    [System.Web.Script.Services.ScriptService]
    public class WS_LLA : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            DAOClub daoC = new DAOClub();

            return "Hello World";
        }


        [WebMethod]
        public string get_clubs(string saisie)
        {
            DAOClub daoc = new DAOClub();

            List<club> lesClubs = new List<club>();
            lesClubs = daoc.getAllClubs(saisie);

            string chaineJSON = "";

            JavaScriptSerializer json_clubs = new JavaScriptSerializer();
            chaineJSON = json_clubs.Serialize(lesClubs);

            return chaineJSON;
        }

        [WebMethod]
        public string get_trous_dun_club(string code)
        {
            DAOTrou daot = new DAOTrou();

            string chaineJSON = "";
            JavaScriptSerializer json_trous = new JavaScriptSerializer();
            chaineJSON = json_trous.Serialize(daot.getAllTrousFromClub(code));

            return chaineJSON;
        }
    }
}
